import sys

from player import Player
from soccer import SoccerGame
from qlearner import QLearner
from friend_q import FriendQ
from foe_q import FoeQ
from ce_q import CeQ

NUM_STATES = 8
NUM_ACTIONS = 5
NUM_PLAYERS = 2
ITERATIONS = 10 ** 6

def q_learner(game, player_a, player_b):
    epsilon = 0.9
    epsilon_min = 0.01
    epsilon_decay = (epsilon - epsilon_min) / ITERATIONS
    alpha_start = 0.5
    alpha = 1.0
    alpha_min = 0.001
    alpha_decay = (alpha - alpha_min) / ITERATIONS
    gamma = 0.9
    ql = QLearner(num_states=NUM_STATES,
                  num_actions=NUM_ACTIONS,
                  num_players=NUM_PLAYERS,
                  alpha=alpha,
                  gamma=gamma,
                  epsilon=epsilon,
                  decay=epsilon_decay,
                  alpha_decay=alpha_decay)

    ql.run(game, ITERATIONS, player_a, player_b)

def friend_q(game, player_a, player_b):

    alpha = 1.0
    alpha_min = 0.001
    alpha_decay = (alpha - alpha_min) / ITERATIONS
    gamma = 0.9

    fq = FriendQ(num_states=NUM_STATES,
                 num_actions=NUM_ACTIONS,
                 num_players=NUM_PLAYERS,
                 alpha=alpha,
                 gamma=gamma,
                 alpha_decay=alpha_decay)

    fq.run(game, ITERATIONS, player_a, player_b)


def foe_q(game, player_a, player_b):

    alpha = 0.9
    alpha_min = 0.001
    alpha_decay = (alpha - alpha_min) / ITERATIONS
    gamma = 0.9

    fq = FoeQ(num_states=NUM_STATES,
              num_actions=NUM_ACTIONS,
              num_players=NUM_PLAYERS,
              alpha=alpha,
              gamma=gamma,
              alpha_decay=alpha_decay)

    fq.run(game, ITERATIONS, player_a, player_b)


def ce_q(game, player_a, player_b):
    alpha = 0.9
    alpha_min = 0.001
    alpha_decay = (alpha - alpha_min) / ITERATIONS
    gamma = 0.9

    cq = CeQ(num_states=NUM_STATES,
             num_actions=NUM_ACTIONS,
             num_players=NUM_PLAYERS,
             alpha=alpha,
             gamma=gamma,
             alpha_decay=alpha_decay)

    cq.run(game, ITERATIONS, player_a, player_b)


if __name__ == '__main__':
    print("\nSelect which algorithm you would like to return:")
    print("[1] Q-Learner")
    print("[2] Friend-Q")
    print("[3] Foe-Q")
    print("[4] Correlated-Q")
    x = int(input(''))

    if x not in {1,2,3,4}:
        print('Invalid input')
        sys.exit()

    player_a = Player(name="A", has_ball=0)
    player_b = Player(name="B", has_ball=1)
    game = SoccerGame(player_a, player_b)

    if x == 1:
        q_learner(game, player_a, player_b)
    elif x == 2:
        friend_q(game, player_a, player_b)
    elif x ==3:
        foe_q(game, player_a, player_b)
    elif x == 4:
        ce_q(game, player_a, player_b)
