import numpy as np
import random

from util import graph_error

class FriendQ:
    """
    alpha == learning rate
    gamma == discount factor
    """
    def __init__(self,
                 num_states,
                 num_actions,
                 num_players,
                 alpha=None,
                 gamma=None,
                 alpha_decay=0,
                 verbose=False):
        self.num_states = num_states
        self.num_actions = num_actions
        self.num_players = num_players
        self.alpha = alpha
        self.gamma = gamma
        self.verbose = verbose
        self.alpha_decay = alpha_decay
        self.initial_state = None

        self.Q = np.random.random([num_states, num_states, num_players,
                                   num_actions, num_actions])

    def determine_action(self):
        """
        This function determines which action to take by using
        the greedy method
        """
        return random.randint(0, self.num_actions-1)

    def set_inital_state(self, player_a_pos, player_b_pos, has_ball,
                         action_a, action_b):
        self.initial_state = player_a_pos, player_b_pos, has_ball,\
                                action_a, action_b

    def get_initial_q_values(self):
        return self.Q[self.initial_state]

    def decay_alpha(self):
        self.alpha -= self.alpha_decay

    def update_q(self, player_a, player_b, has_ball, action_a, action_b,
                 reward_a, new_state_a, new_state_b, new_state_ball):
        """
        This is where the main update takes place.

        Note: if we've reached the terminal state, the max of the new state
                should be set to 0

        """
        self.Q[player_a, player_b, has_ball, action_a, action_b] = \
                    (1 - self.alpha) * self.Q[player_a, player_b,
                                               has_ball, action_a, action_b] + \
                    self.alpha * ((1 - self.gamma) * reward_a + \
                                  self.gamma * np.max(self.Q[new_state_a,
                                                             new_state_b,
                                                             new_state_ball]))

    def run(self, soccer_game, iterations, player_a, player_b):
        soccer_game.new_game()
        done = 0
        error_list = []
        iteration_list = []

        for i in range(iterations):
            if done == 1:
                soccer_game.new_game()
                done = 0

            player_a_loc, player_b_loc = soccer_game.get_player_positions(player_a,
                                                                          player_b)
            who_has_ball = soccer_game.ball
            action_a = self.determine_action()
            action_b = self.determine_action()

            if self.initial_state is None:
                self.set_inital_state(player_a_loc, player_b_loc, who_has_ball,
                                      action_a, action_b)

            initial_q_val = self.get_initial_q_values()
            new_state, reward_a, reward_b, done = soccer_game.next_step(actionA=action_a,
                                                                        actionB=action_b)
            new_pos_a, new_pos_b, new_has_ball = new_state

            self.update_q(player_a_loc, player_b_loc, who_has_ball, action_a,
                          action_b, reward_a, new_pos_a, new_pos_b, new_has_ball)

            if [player_a_loc, player_b_loc, who_has_ball, action_a, action_b] == list(self.initial_state):
                error_list.append(abs(self.Q[self.initial_state] - initial_q_val))
                iteration_list.append(i)
                print("Iteration: ", i, self.alpha)

            self.decay_alpha()

        graph_error(error_list, iteration_list, name="Friend Q Learning")
