# Correlated-Q Learning Replication Project
## How to my program
Make sure to run this command on terminal
`pip install -r requirements.txt`

Once you have successfully installed the requirements, you can run
`python analysis.py`
Which will prompt a selection:
```
Select which algorithm you would like to return:
[1] Q-Learner
[2] Friend-Q
[3] Foe-Q
[4] Correlated-Q
```

Go ahead and select which algorithm you would like to run by selecting the number.
