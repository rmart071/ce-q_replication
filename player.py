class Player(object):
    def __init__(self, name, has_ball=None):
        self.name = name
        self.position = 0
        self.has_ball = has_ball

    def has_ball(self):
        return self.has_ball
