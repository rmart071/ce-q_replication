import numpy as np
import matplotlib.pyplot as plt
from cvxopt import matrix, solvers
from numpy import array, eye, hstack, ones, vstack, zeros

def graph_error(diff_list, iterations, name, linewidth = 1):
    plt.plot(iterations, diff_list, linewidth = linewidth)
    plt.ticklabel_format(style='sci', axis='x', scilimits=(0, 0))
    plt.title(name)
    plt.xlabel('Iteration')
    plt.ylabel('Q Value Difference')
    plt.ylim(0, 0.5)
    plt.savefig('images/{}.png'.format(name))
    plt.gcf().clear()


def maximin_lp(q_values):
    solver = 'glpk'
    solvers.options['glpk'] = {'msg_lev': 'GLP_MSG_OFF'}
    solvers.options['LPX_K_MSGLEV'] = 0
    solvers.options['show_progress'] = False

    q_values_transposed = matrix(q_values).trans()
    size_of_matrix = q_values_transposed.size[1]

    A = hstack((ones((q_values_transposed.size[0], 1)), q_values_transposed))
    ones_matrix = hstack((zeros((size_of_matrix, 1)), -eye(size_of_matrix)))

    A = vstack((A, ones_matrix))
    A = matrix(vstack((A, hstack((0,ones(size_of_matrix))),
                       hstack((0,-ones(size_of_matrix))))))

    b = matrix(hstack((zeros(A.size[0] - 2), [1, -1])))
    c = matrix(hstack(([-1], zeros(size_of_matrix))))
    sol = solvers.lp(c,A,b, solver=solver)

    return sol['primal objective']


def build_constraints(matrix_size, qa, qb):
    row = 0
    matrix_size_2 = matrix_size**2
    matrix_minus_one = matrix_size - 1
    constraints = np.zeros((2 * matrix_size * (matrix_minus_one),
                            (matrix_size_2)))

    for i in range(matrix_size):
        for j in range(matrix_size):
            if i != j:
                single_row_diff = qa[i] - qa[j]
                multi_row_dff = qb[:, i] - qb[:, j]
                constraints[row,
                            i * matrix_size:(i + 1) * matrix_size] = \
                            single_row_diff
                constraints[row + matrix_size * (matrix_minus_one),
                            i:(matrix_size_2):matrix_size] = multi_row_dff
                row += 1
    return constraints



def ceq_lp(qa_values, qb_values):
    solver = 'glpk'
    solvers.options['glpk'] = {'msg_lev': 'GLP_MSG_OFF'}
    solvers.options['LPX_K_MSGLEV'] = 0
    solvers.options['show_progress'] = False

    matrix_size = matrix(qa_values).trans().size[1]
    matrix_size_2 = matrix_size**2
    qa = np.array(qa_values)
    qb = np.array(qb_values)

    constraints = build_constraints(matrix_size, qa, qb)
    A = matrix(constraints)
    A = hstack((ones((A.size[0], 1)), A))
    ones_matrix = hstack((zeros((matrix_size_2, 1)),
                          -eye(matrix_size_2)))

    A = vstack((A, ones_matrix))
    A = matrix(vstack((A, hstack((0,ones(matrix_size_2))),
                       hstack((0,-ones(matrix_size_2))))))

    b = matrix(hstack((zeros(A.size[0] - 2), [1, -1])))
    c = matrix(hstack(([-1.], -(np.add(qa, qb)).flatten())))
    sol = solvers.lp(c,A,b, solver=solver)

    if sol['x'] is not None:
        dist = sol['x'][1:]
        qa_flat = qa.flatten()
        qb_flat = qb.transpose().flatten()

        qa_exp = np.matmul(qa_flat, dist)[0]
        qb_exp = np.matmul(qb_flat, dist)[0]
    else:
        qa_exp = 0
        qb_exp = 0

    return qa_exp, qb_exp
