import numpy as np
import random

from util import graph_error

class QLearner:
    """
    alpha == learning rate
    gamma == discount factor
    """
    def __init__(self,
                 num_states,
                 num_actions,
                 num_players,
                 alpha=None,
                 gamma=None,
                 epsilon=None,
                 decay=0,
                 alpha_decay=0,
                 verbose=False):
        self.num_states = num_states
        self.num_actions = num_actions
        self.num_players = num_players
        self.alpha = alpha
        self.gamma = gamma
        self.epsilon = epsilon
        self.verbose = verbose
        self.decay = decay
        self.alpha_decay = alpha_decay
        self.initial_state = None
        self.initial_state_with_both_actions = None
        self.Qa = np.zeros([num_states, num_states,
                           num_players, num_actions])
        self.Qb = np.zeros([num_states,num_states,
                           num_players, num_actions])

    def determine_action(self, player_a_pos, player_b_pos, has_ball):
        """
        This function determines which action to take by using
        the greedy method
        """
        # Epsilon Greedy Search
        if self.epsilon > np.random.random():
            actionA = np.random.choice(self.num_actions)
            actionB = np.random.choice(self.num_actions)
        else:
            actionA = np.argmax(self.Qa[player_a_pos, player_b_pos, has_ball])
            actionB = np.random.choice(self.num_actions)

        return actionA, actionB

    def set_inital_state(self, player_a_pos, player_b_pos, has_ball, action_a):
        self.initial_state = player_a_pos,player_b_pos, has_ball, action_a

    def set_state_two_actions(self, action_b):
        final_tuple = self.initial_state + (action_b,)
        self.initial_state_with_both_actions = list(final_tuple)

    def get_inital_state(self):
        return self.initial_state

    def get_initial_q_values(self):
        return self.Qa[self.initial_state]

    def decay_epsilon(self):
        """
        This function applies decay if meets the correct logic
        """
        if self.epsilon > 0.001:
            self.epsilon -= self.decay

    def decay_alpha(self):
        self.alpha -= self.alpha_decay

    def update_q(self, player_a, player_b, has_ball,
                 action_a, reward_a, new_state_a, new_state_b, new_state_ball):
        """
        This is where the main update takes place.

        Note: if we've reached the terminal state, the max of the new state
                should be set to 0

        """
        self.Qa[player_a, player_b, has_ball, action_a] = \
                    (1 - self.alpha) * self.Qa[player_a, player_b,
                                               has_ball, action_a] + \
                    self.alpha * ((1 - self.gamma) * reward_a + \
                                  self.gamma * np.max(self.Qa[new_state_a,
                                                              new_state_b,
                                                              new_state_ball]))

    def run(self, soccer_game, iterations, player_a, player_b):
        soccer_game.new_game()
        done = 0
        error_list = []
        iteration_list = []

        for i in range(iterations):
            if done == 1:
                soccer_game.new_game()

            player_a_loc, player_b_loc = soccer_game.get_player_positions(player_a,
                                                                          player_b)
            who_has_ball = soccer_game.ball
            action_a, action_b = self.determine_action(player_a_loc,
                                                       player_b_loc,
                                                       who_has_ball)
            if self.get_inital_state() is None:
                self.set_inital_state(player_a_loc, player_b_loc, who_has_ball, action_a)
                self.set_state_two_actions(action_b)

            initial_q_val = self.get_initial_q_values()


            new_state, reward_a, reward_b, done = soccer_game.next_step(actionA=action_a,
                                                                        actionB=action_b)
            new_pos_a, new_pos_b, new_has_ball = new_state

            self.update_q(player_a_loc, player_b_loc, who_has_ball,
                          action_a, action_b, new_pos_a, new_pos_b,
                          new_has_ball)

            if [player_a_loc, player_b_loc, who_has_ball, action_a, action_b] == \
                                self.initial_state_with_both_actions:
                error_list.append(abs(self.Qa[self.initial_state] - initial_q_val))
                iteration_list.append(i)
                print("Iteration: ", i, self.alpha)
            self.decay_alpha()

        graph_error(error_list, iteration_list, name="Q-Learner", linewidth=0.5)
