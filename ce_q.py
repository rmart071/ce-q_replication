import numpy as np
import random

from util import graph_error, ceq_lp

class CeQ:
    """
    alpha == learning rate
    gamma == discount factor
    """
    def __init__(self,
                 num_states,
                 num_actions,
                 num_players,
                 alpha=None,
                 gamma=None,
                 alpha_decay=0,
                 verbose=False):
        self.num_states = num_states
        self.num_actions = num_actions
        self.num_players = num_players
        self.alpha = alpha
        self.gamma = gamma
        self.verbose = verbose
        self.alpha_decay = alpha_decay
        self.Qa = np.zeros([num_states, num_states, num_players,
                                   num_actions, num_actions])
        self.Qb = np.zeros([num_states, num_states, num_players,
                                   num_actions, num_actions])

    def determine_action(self):
        """
        This function determines which action to take by using
        the greedy method
        """
        return random.randint(0, self.num_actions-1)

    def get_initial_q_values(self):
        return self.Qa[2, 1, 1, 2, 4]

    def decay_alpha(self):
        self.alpha -= self.alpha_decay

    def calculate_alpha(self):
        self.alpha *= np.e ** (-np.log(500.0) / 10 ** 6)

    def update_q(self, player_a, player_b, has_ball, action_a, action_b,
                 reward_a, reward_b, exp_A, exp_b):
        """
        This is where the main update takes place.

        Note: if we've reached the terminal state, the max of the new state
                should be set to 0

        """
        self.Qa[player_a, player_b, has_ball, action_a, action_b] = \
                    (1 - self.alpha) * self.Qa[player_a, player_b,
                                               has_ball, action_a, action_b] + \
                    self.alpha * ((1 - self.gamma) * reward_a + \
                                  self.gamma * exp_A)

        self.Qb[player_a, player_b, has_ball, action_a, action_b] = \
                    (1 - self.alpha) * self.Qb[player_a, player_b,
                                               has_ball, action_a, action_b] + \
                    self.alpha * ((1 - self.gamma) * reward_b + \
                                  self.gamma * exp_b)

    def run(self, soccer_game, iterations, player_a, player_b):
        soccer_game.new_game()
        done = 0
        error_list = []
        iteration_list = []

        for i in range(iterations):
            if done == 1:
                soccer_game.new_game()
                done = 0

            player_a_loc, player_b_loc =soccer_game.get_player_positions(player_a, player_b)
            who_has_ball = soccer_game.ball
            initial_q_values = self.get_initial_q_values()
            action_a = self.determine_action()
            action_b = self.determine_action()

            Qa_state = self.Qa[player_a_loc, player_b_loc, who_has_ball]
            Qb_state = self.Qb[player_a_loc, player_b_loc, who_has_ball]

            new_state, reward_a, reward_b, done = soccer_game.next_step(actionA=action_a,
                                                                        actionB=action_b)
            new_pos_a, new_pos_b, new_has_ball = new_state
            exp_a, exp_b = ceq_lp(Qa_state, Qb_state)

            self.update_q(player_a_loc, player_b_loc, who_has_ball, action_a,
                          action_b, reward_a, reward_b, exp_a, exp_b)

            # we will check values this state is in place: 2, 1, 1, 2, 4
            if [player_a_loc, player_b_loc, who_has_ball, action_a, action_b] == [2, 1, 1, 2, 4]:
                error_list.append(abs(self.Qa[2, 1, 1, 2, 4] - initial_q_values))
                iteration_list.append(i)
                print("Iteration: ", i, self.alpha)

            self.calculate_alpha()
        graph_error(error_list, iteration_list, name="Correlated Equilibirum Q Learning")
