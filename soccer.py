import numpy as np
import copy
import random

from player import Player

class SoccerGame(object):
    def __init__(self, player_a, player_b):
        self.player_a = player_a
        self.player_b = player_b
        self.rows = 2
        self.columns = 4
        self.goal_states_a = [0, 4]
        self.goal_states_b = [3, 7]
        self.ball = np.random.randint(2)
        self.ball_pos = player_a.position

    def new_game(self, random_start = True):
        starting_positions = [1, 2, 5, 6]
        random_int = np.random.choice(len(starting_positions), 2, replace=False)

        if random_start:
            # Random Player Position
            self.player_a.position = starting_positions[random_int[0]]
            self.player_b.position = starting_positions[random_int[1]]

            # Ball starts randomly in one player's possesion
            if np.random.randint(2) == 0:
                self.ball = self.player_a.has_ball
                self.ball_pos = self.player_a.position
            else:
                self.ball = self.player_b.has_ball
                self.ball_pos = self.player_b.position

        else:
            #Start in Selected State
            self.player_a.position = 2
            self.player_b.position = 1
            self.ball = self.player_b.has_ball
            self.ball_pos = self.player_b.position

    def move_player(self, player, action):
        # Move North
        if action == 0 and player.position > 3:
            player_loc = player.position - 4
        # Move East
        elif action == 1 and player.position not in self.goal_states_b:
            player_loc = player.position + 1
        # Move South
        elif action == 2 and player.position < 4:
            player_loc = player.position + 4
        # Move West
        elif action == 3 and player.position not in self.goal_states_a:
            player_loc = player.position - 1
        # Stick
        else:
            player_loc = player.position

        return player_loc

    def actions(self, player1, player2, action1, action2):

        temp_loc1 = self.move_player(player1, action1)
        temp_loc2 = self.move_player(player2, action2)

        # Player 1 moves first every time
        if temp_loc1 != player2.position:
            # Move to empty spot
            player1.position = temp_loc1
        else:
            # Transfer Ball if location is already occupied
            self.ball = player2.has_ball

        if temp_loc2 != player1.position:
            player2.position = temp_loc2
        else:
            self.ball = player1.has_ball

        #Move Ball with player
        if self.ball:
            self.ball_pos = self.player_a.position
        else:
            self.ball_pos = self.player_b.position

    def next_step(self, actionA, actionB):

        player_a = self.player_a
        player_b = self.player_b

        # First Mover chosen randomly
        if np.random.randint(2) == 0:
            self.actions(player_a, player_b, actionA, actionB)
        else:
            self.actions(player_b, player_a, actionB, actionA)

        # Goal Scored, End Game
        if self.ball_pos in self.goal_states_a:
            rewardA = 100
            rewardB = -100
            done = 1
            # print("Game Over, Ball in A Goal")
        elif self.ball_pos in self.goal_states_b:
            rewardA = -100
            rewardB = 100
            done = 1
            # print("Game Over, Ball in B Goal")
        else:
            rewardA = 0
            rewardB = 0
            done = 0

        return self.state(), rewardA, rewardB, done


    def state(self):
        return [self.player_a.position, self.player_b.position, self.ball]

    def get_player_positions(self, player_a, player_b):
        return player_a.position, player_b.position
